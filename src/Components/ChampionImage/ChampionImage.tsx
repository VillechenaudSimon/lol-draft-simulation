import React from "react";
import { Champion, NULL_CHAMPION } from "Structures/Champion";

type PropsType = {
    champion: Champion,
    rounded?: string,
};

export default function ChampionImage(props: PropsType) {
    let img = <></>
    if (props.champion !== NULL_CHAMPION) {
        img = (<img
            className={`select-none w-full h-full${(props.rounded !== undefined) ? ` rounded-${props.rounded}` : ""}`}
            src={`https://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/v1/champion-icons/${props.champion.id}.png`}
            alt={props.champion.name}
            draggable="false"
        />)
    }
    return (
        <>
            {img}
        </>
    );
} 
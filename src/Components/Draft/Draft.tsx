import ChampionList from "Components/ChampionList/ChampionList";
import React from "react";
import { Champion, NULL_CHAMPION } from "Structures/Champion";
import Team from "Components/Team/Team";

type PropsType = {

};


export let SelectedChampionStateContext = React.createContext<{
    selectedChampion: Champion,
    setSelectedChampion: React.Dispatch<React.SetStateAction<Champion>>
}>({
    selectedChampion: NULL_CHAMPION,
    setSelectedChampion: () => { }
});

export default function Draft(props: PropsType) {
    const [selectedChampion, setSelectedChampion] = React.useState(NULL_CHAMPION);
    
    return (
        <SelectedChampionStateContext.Provider value={{ selectedChampion, setSelectedChampion }}>
            <Team color="blue" />
            <ChampionList />
            <Team color="red" />
        </SelectedChampionStateContext.Provider>
    );
}
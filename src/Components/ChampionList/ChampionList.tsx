import React from "react";
import ChampionSquare from "Components/ChampionSquare/ChampionSquare";
import { Champion } from "Structures/Champion";
import { useQuery } from "@tanstack/react-query";
import fetchChampions from "ChampionFetcher/ChampionFetcher";

export default function ChampionList() {
    const { isLoading, error, data, isFetching } = useQuery<Champion[], Error>(["championList"], () => fetchChampions(), {staleTime: Infinity});

    function champions() {
        if (isLoading) return (<span>Loading..</span>)

        if (error) return (<span>An error occured fetching champion list : {error.message}</span>)

        if (isFetching) return (<>Updating..</>)

        return <> {data.map((champion, i) => {
            if (champion.id > 0)
                return (<ChampionSquare key={i} champion={champion} />)
            return null
        })}</>
    }

    return (
        <div tabIndex={-1} className="w-full h-full bg-transparent grid grid-cols-fit-20 gap-14 p-14 overflow-y-auto scrollbar-hide">
            {champions()}
        </div>
    );
}
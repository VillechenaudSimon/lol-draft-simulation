import { SelectedChampionStateContext } from "Components/Draft/Draft";
import ChampionImage from "Components/ChampionImage/ChampionImage";
import React from "react";
import { Champion, NULL_CHAMPION } from "Structures/Champion";

type PropsType = {
    champion: Champion,
};

export default function ChampionSquare(props: PropsType) {
    const SelectedChampionState = React.useContext(SelectedChampionStateContext);
    const isSelected = () => {
        return SelectedChampionState.selectedChampion===props.champion;
    }

    const handleClick = () => {
        if (isSelected()) {
            SelectedChampionState.setSelectedChampion(NULL_CHAMPION)
        } else {
            SelectedChampionState.setSelectedChampion(props.champion);
        }
    }
    return (
        <div onClick={handleClick} className={`cursor-pointer bg-black ${isSelected() ? `border-yellow-400 border-2` : ``} `}>
            <div>
                <ChampionImage champion={props.champion} />
            </div>
        </div>
    );
}
import PlayerPlaceholder from "Components/PlayerPlaceholder/PlayerPlaceholder";
import React from "react";

type PropsType = {
    color?: string
};

export default function Team({ color }: PropsType) {
    const players = Array(5).fill(0).map((_, i) => <PlayerPlaceholder key={i} />);

    return (
        <div className="bg-slate-600 p-4 w-full h-full flex flex-col justify-evenly items-center">
            {players}
        </div>
    );
}
import React from 'react';
import Draft from 'Components/Draft/Draft';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const queryClient = new QueryClient();

type PropsType = {
  className?: string
};

export default function App({ className }: PropsType) {

  const [showDraft, setShowDraft] = React.useState(true);

  return (
    <QueryClientProvider client={queryClient}>
      <div className={`${className ? `${className} ` : ""}App text-center bg-slate-700 font-bold grid grid-cols-[17%_66%_17%]`}>
        {
          showDraft ? <Draft /> : <></>
        }
      </div>
    </QueryClientProvider>
  );
}

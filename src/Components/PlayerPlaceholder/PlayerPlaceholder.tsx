import { SelectedChampionStateContext } from "Components/Draft/Draft";
import ChampionImage from "Components/ChampionImage/ChampionImage";
import React from "react";
import { NULL_CHAMPION } from "Structures/Champion";

export default function PlayerPlaceholder() {
    const [champion, setChampion] = React.useState<{
        alias: string,
        id: number,
        name: string,
    }>(NULL_CHAMPION);

    const SelectedChampionState = React.useContext(SelectedChampionStateContext);

    const handleClick = () => {
        setChampion(SelectedChampionState.selectedChampion);
        SelectedChampionState.setSelectedChampion(NULL_CHAMPION);
    };

    const computeImage = () => {
        if (champion.id !== undefined)
            return <ChampionImage champion={champion} rounded={"full"} />
        return <div></div>
    }

    return (
        <div onClick={handleClick} className="cursor-pointer bg-gray-900 h-[14%] aspect-square">
            {computeImage()}
        </div>
    );
}
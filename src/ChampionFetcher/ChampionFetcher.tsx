import { Champion } from "Structures/Champion";

/**
 * Fetch league of legends champion list and sort them by name (alphabetic order)
 * @returns Promise<Champion[]>
 */
export default function fetchChampions() {
    return fetch("https://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/v1/champion-summary.json")
        .then((data) => data.json())
        .then((data: Array<Champion>) => {
            return data.sort(
                (a, b) => a.name.localeCompare(b.name)
            )
        });
}
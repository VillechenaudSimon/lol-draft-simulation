export type Champion = {
    alias: string,
    id: number,
    name: string,
};

export const NULL_CHAMPION: Champion = {
    alias: "",
    id: -1,
    name: "",
};
/** @type {import('tailwindcss').Config} */

let gridTemplateColumnsFit = {}
for(let i = 3; i < 7; i++) {
  gridTemplateColumnsFit[`fit-${4*i}`] = `repeat(auto-fit, minmax(${i}rem, 1fr))`;
}

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'dark-metal': '#282c34',
      },
      gridTemplateColumns: {
        // Simple auto-fit column grid
        ...gridTemplateColumnsFit
      },
    },
  },
  plugins: [
    require('tailwind-scrollbar-hide')
  ],
}
